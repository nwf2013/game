package casino

import (
	"fmt"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
)

const (
	WMZR = "WMZR"
)

var wmzrLang = map[string]string{
	"cn": "0",
	"vn": "3",
}

//完美真人注册
func wmzrReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	//内部语言映射

	args := map[string]string{
		"vendorId":  param["vendor_id"].(string),
		"signature": param["signature"].(string),
		"user":      param["prefix"].(string) + param["username"].(string),
		"password":  param["password"].(string),
		"username":  param["prefix"].(string) + param["username"].(string),
		"syslang":   wmzrLang[param["lang"].(string)],
		"timestamp": param["s"].(string),
	}
	//头像 (非必要)
	if _, ok := param["profile"].(string); ok {
		args["profile"] = param["profile"].(string)
	}
	//最大可赢 (非必要)
	if _, ok := param["maxwin"].(string); ok {
		args["maxwin"] = param["maxwin"].(string)
	}
	//最大可输 (非必要)
	if _, ok := param["maxlose"].(string); ok {
		args["maxlose"] = param["maxlose"].(string)
	}
	//备注 最大值:20(非必要)
	if _, ok := param["mark"].(string); ok {
		args["mark"] = param["mark"].(string)
	}
	//會員退水是否歸零 0為:不歸零 1為:歸零(非必要)
	if _, ok := param["rakeback"].(string); ok {
		args["rakeback"] = param["rakeback"].(string)
	}
	//限制类型 例:2,5,9,14,48,107,111,131 (非必要)
	if _, ok := param["limit_type"].(string); ok {
		args["limitType"] = param["limit_type"].(string)
	}
	//会员筹码 使用逗号隔开，可填入5-10组 (非必要)
	//若没带入，会员筹码预设为 10,20,50,100,500,1000,5000,10000,20000,50000
	//可用筹码种类：1, 5, 10, 20, 50, 100, 500, 1000, 5000, 10000, 20000, 50000, 100000, 200000, 1000000, 5000000, 10000000, 20000000, 50000000, 10000000, 20000000, 50000000, 100000000
	if _, ok := param["chip"].(string); ok {
		args["chip"] = param["chip"].(string)
	}
	requestURI := fmt.Sprintf("%s?cmd=MemberRegister&%s", param["api"].(string), paramEncode(args))

	statusCode, body, err := httpGetWithPushLog(zlog, param["username"].(string), WMZR, requestURI)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("errorCode") == 0 {
		return Success, "success"
	}

	return Failure, string(v.GetStringBytes("errorMessage"))
}

//完美真人登录
func wmzrLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"vendorId":  param["vendor_id"].(string),
		"signature": param["signature"].(string),
		"user":      param["prefix"].(string) + param["username"].(string),
		"password":  param["password"].(string),
		"lang":      wmzrLang[param["lang"].(string)],
		"timestamp": param["s"].(string),
	}

	//onlybac 略过大厅直接进入百家乐
	//onlydgtg 略过大厅直接进入龙虎
	//onlyrou 略过大厅直接进入轮盘
	//onlysicbo 略过大厅直接进入骰宝
	//onlyniuniu 略过大厅直接进入牛牛
	//onlysamgong 略过大厅直接进入三公
	//onlyfantan 略过大厅直接进入番摊
	//onlysedie 略过大厅直接进入色碟
	//onlyfishshrimpcrab 略过大厅直接进入鱼虾蟹
	//onlygoldenflower 略过大厅直接进入炸金花
	//onlypaigow 略过大厅直接进入温州牌九
	//onlythisbar 略过大厅直接进入二八杠
	//onlyandarbahar 略过大厅直接进入安達巴哈
	if _, ok := param["mode"].(string); ok {
		args["mode"] = param["mode"].(string)
	}
	//登出wm時，返回地址 (非必要)
	if _, ok := param["return_url"].(string); ok {
		args["returnurl"] = param["return_url"].(string)
	}
	//1:試玩 (非必要)
	if _, ok := param["isTest"].(string); ok {
		args["isTest"] = param["isTest"].(string)
	}
	//1:iframe嵌入 (非必要)
	if _, ok := param["size"].(string); ok {
		args["size"] = param["size"].(string)
	}
	//0:经典版, 2:棋牌风格 (非必要)
	if _, ok := param["ui"].(string); ok {
		args["ui"] = param["ui"].(string)
	}
	//9:微信专用 (非必要)
	if _, ok := param["site"].(string); ok {
		args["site"] = param["site"].(string)
	}
	//0:中文, 1:英文 (非必要)
	if _, ok := param["syslang"].(string); ok {
		args["syslang"] = param["syslang"].(string)
	}
	requestURI := fmt.Sprintf("%s?cmd=SigninGame&%s", param["api"].(string), paramEncode(args))

	statusCode, body, err := httpGetWithPushLog(zlog, param["username"].(string), WMZR, requestURI)
	if err != nil {

		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {

		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("errorCode") == 0 {
		return Success, string(v.GetStringBytes("result"))
	}

	return Failure, string(v.GetStringBytes("errorMessage"))
}

//完美真人余额
func wmzrBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	args := map[string]string{
		"vendorId":  param["vendor_id"].(string),
		"signature": param["signature"].(string),
		"user":      param["prefix"].(string) + param["username"].(string),
		"syslang":   param["syslang"].(string),
		"timestamp": param["s"].(string),
	}
	requestURI := fmt.Sprintf("%s?cmd=GetBalance&%s", param["api"].(string), paramEncode(args))

	statusCode, body, err := httpGetWithPushLog(zlog, param["username"].(string), WMZR, requestURI)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("errorCode") == 0 {
		return Success, GetBalanceFromFloat(v.GetFloat64("result"))
	}

	return Failure, string(v.GetStringBytes("errorMessage"))
}

//完美真人上下分
func wmzrTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var (
		p      fastjson.Parser
		amount string
	)
	if param["type"].(string) == "in" {
		amount = param["amount"].(string)
	} else {
		amount = "-" + param["amount"].(string)
	}
	args := map[string]string{
		"vendorId":  param["vendor_id"].(string),
		"signature": param["signature"].(string),
		"user":      param["prefix"].(string) + param["username"].(string),
		"password":  param["password"].(string),
		"money":     amount,
		"order":     param["id"].(string), //我司转账订单号
		"timestamp": param["s"].(string),
		"syslang":   param["syslang"].(string),
	}
	requestURI := fmt.Sprintf("%s?cmd=ChangeBalance&%s", param["api"].(string), paramEncode(args))

	statusCode, body, err := httpGetWithPushLog(zlog, param["username"].(string), WMZR, requestURI)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("errorCode") == 0 {

		vv := v.Get("result")
		return Success, string(vv.GetStringBytes("orderId"))
	}

	return Failure, string(v.GetStringBytes("errorMessage"))
}

//转账确认
func wmzrConfirm(zlog *fluent.Fluent, param map[string]interface{}) bool {

	return false
}
