package casino

import (
	"fmt"
	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
)

const FYDJ string = "FYDJ"

var fydjLang = map[string]string{
	"cn": "CHN",
	"vn": "VN",
}

var fydyCurrency = map[string]string{
	"cn": "CNY",
	"vn": "VND",
}

// 注册
func fydjReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	lang := param["lang"].(string)

	args := map[string]string{
		"UserName": param["prefix"].(string) + param["username"].(string),
		"Password": param["password"].(string),
		"Currency": fydyCurrency[lang],
	}

	requestURI := fmt.Sprintf("%s/api/user/register", param["api"].(string))
	str := paramEncode(args)
	header := map[string]string{
		"Content-Type":     "application/x-www-form-urlencoded",
		"Content-Language": fydjLang[lang],
		"Authorization":    param["key"].(string),
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), param["username"].(string), FYDJ, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("success") == 1 {
		return Success, "success"
	}

	return Failure, string(v.GetStringBytes("msg"))
}

// 登录
func fydjLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	lang := param["lang"].(string)

	gameID := param["gamecode"].(string)

	args := map[string]string{
		"UserName": param["prefix"].(string) + param["username"].(string),
		"CateID":   gameID, //可选，要进入的游戏。如果留空或者游戏ID错误，则默认进入游戏大厅首页，游戏ID代码请参见附录1
		"MatchID":  "",     //可选，要进入的赛事。如果留空或者比赛ID错误，则默认进入游戏大厅首页。当前的赛事可通过 3.1接口进行获取。
	}

	requestURI := fmt.Sprintf("%s/api/user/login", param["api"].(string))
	str := paramEncode(args)
	header := map[string]string{
		"Content-Type":     "application/x-www-form-urlencoded",
		"Content-Language": fydjLang[lang],
		"Authorization":    param["key"].(string),
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), param["username"].(string), FYDJ, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("success") == 1 {
		vv := v.Get("info")
		return Success, string(vv.GetStringBytes("Url"))
	}

	return Failure, string(v.GetStringBytes("msg"))
}

// 余额
func fydjBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	lang := param["lang"].(string)

	args := map[string]string{
		"UserName": param["prefix"].(string) + param["username"].(string),
	}

	requestURI := fmt.Sprintf("%s/api/user/balance", param["api"].(string))
	str := paramEncode(args)
	header := map[string]string{
		"Content-Type":     "application/x-www-form-urlencoded",
		"Content-Language": fydjLang[lang],
		"Authorization":    param["key"].(string),
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), param["username"].(string), FYDJ, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("success") == 1 {
		vv := v.Get("info")
		balance,_ := GetBalanceFromByte(vv.GetStringBytes("Money"))
		return Success, balance
	}

	return Failure, string(v.GetStringBytes("msg"))
}

// 转帐
func fydjTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	lang := param["lang"].(string)

	method := "IN"
	if param["type"] == "out" {
		method = "OUT"
	}

	args := map[string]string{
		"UserName": param["prefix"].(string) + param["username"].(string),
		"Type":     method,
		"Money":    param["amount"].(string),
		"ID":       param["id"].(string),
		"Currency": fydyCurrency[lang],
	}

	requestURI := fmt.Sprintf("%s/api/user/transfer", param["api"].(string))
	str := paramEncode(args)
	header := map[string]string{
		"Content-Type":     "application/x-www-form-urlencoded",
		"Content-Language": fydjLang[lang],
		"Authorization":    param["key"].(string),
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), param["username"].(string), FYDJ, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("success") == 1 {
		return Success, param["id"].(string)
	}

	return Failure, string(v.GetStringBytes("msg"))
}

// 确认
func fydjConfirm(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	return Success, "success"
}
