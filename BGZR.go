package casino

import (
	"fmt"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"github.com/wI2L/jettison"
)

const BGZR string = "BGZR"

var bgzrLang = map[string]string{
	"cn": "zh_CN",
	"vn": "vi_VN",
}

//BG真人
func bgzrPack(args map[string]string,param map[string]interface{}, md5Str,id, method string) []byte {

	secretCode := Base64Encode(Sha1Sum(param["pwd"].(string)))
	digest := getMD5Hash(md5Str + secretCode)
	args["digest"] = digest
	data := map[string]interface{}{
		"id":      id,
		"method":  method,
		"params":  args,
		"jsonrpc": "2.0",
	}
	bs, _ := jettison.Marshal(data)

	return bs
}

// 注册
func bgzrReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	rd := randomString(32)
	md5Str := rd + param["sn"].(string)

	method := "open.user.create"
	requestURI := fmt.Sprintf("%s/%s", param["api"].(string), method)
	header := map[string]string{
		"Content-Type": "application/json",
	}
	args := map[string]string{
		"random":       rd,
		"sn":           param["sn"].(string),
		"agentLoginId": param["agent_id"].(string),
		"loginId":      param["prefix"].(string) + param["username"].(string),
	}
	requestBody := bgzrPack(args,param, md5Str,rd, method)

	statusCode, body, err := httpPostWithPushLog(zlog, requestBody, param["username"].(string), BGZR, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	errObject := v.Get("error")

	if errObject.String() == "null" || string(errObject.GetStringBytes("code")) == "2206" {
		return Success, "success"
	}

	return Failure, string(errObject.GetStringBytes("message"))
}

// 登陆
func bgzrLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	rd := randomString(32)
	isMobileUrl := "1"
	if param["deviceType"].(string) == "1"{
		isMobileUrl = "0"
	}
	md5Str := rd + param["sn"].(string) + param["prefix"].(string) + param["username"].(string)
	method := "open.video.game.url"
	args := map[string]string{
		"random":  rd,
		"sn":      param["sn"].(string),
		"loginId": param["prefix"].(string) + param["username"].(string),
		"locale":bgzrLang[param["lang"].(string)],
		"isMobileUrl":isMobileUrl,
	}
	requestBody := bgzrPack(args,param, md5Str,rd, method)
	requestURI := fmt.Sprintf("%s/%s", param["api"].(string), method)
	header := map[string]string{
		"Content-Type": "application/json",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, requestBody, param["username"].(string), BGZR, requestURI, header)
	if err != nil {

		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if err := v.GetObject("error"); err != nil {
		return Failure, err.Get("message").String()
	}

	return Success, string(v.GetStringBytes("result"))
}

// 查询余额
func bgzrBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var (
		p fastjson.Parser
	)
	rd := randomString(32)
	md5Str := rd + param["sn"].(string) + param["prefix"].(string) + param["username"].(string)

	method := "open.balance.get"
	requestURI := fmt.Sprintf("%s/%s", param["api"].(string), method)
	header := map[string]string{
		"Content-Type": "application/json",
	}

	args := map[string]string{
		"random":  rd,
		"sn":      param["sn"].(string),
		"loginId": param["prefix"].(string) + param["username"].(string),
	}
	requestBody := bgzrPack(args,param, md5Str,rd, method)

	statusCode, body, err := httpPostWithPushLog(zlog, requestBody, param["username"].(string), BGZR, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if err := v.GetObject("error"); err != nil {
		return Failure, err.Get("message").String()
	}

	return Success, GetBalanceFromFloat(v.GetFloat64("result"))
}

// 上下分
func bgzrTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	rd := randomString(32)

	args := map[string]string{
		"random":     rd,
		"sn":         param["sn"].(string),
		"loginId":    param["prefix"].(string) + param["username"].(string),
		"checkBizId": "1",
		"bizId":      param["id"].(string),
	}

	if param["type"].(string) == "in" {
		args["amount"] = param["amount"].(string)
	} else {
		args["amount"] = "-" + param["amount"].(string)
	}
	md5Str := rd + param["sn"].(string) + param["prefix"].(string) + param["username"].(string) + args["amount"]

	method := "open.balance.transfer"
	requestURI := fmt.Sprintf("%s/%s", param["api"].(string), method)
	header := map[string]string{
		"Content-Type": "application/json",
	}
	requestBody := bgzrPack(args,param, md5Str,rd, method)
	statusCode, body, err := httpPostWithPushLog(zlog, requestBody, param["username"].(string), BGZR, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	errObj := v.GetObject("error")
	if errObj == nil {

		return Success, param["id"].(string)
	}

	return Failure, errObj.Get("message").String()
}
