package casino

import (
	"fmt"
	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
)

const IBC = "IBC"

var ibcCurrency = map[string]string{
	"cn":"RMB",
	"vn":"51",
}

var ibcLang = map[string]string{
	"cn":"cs",
	"vn":"vn",
}

var ibcHead = map[string]string{
	"Content-Type": "application/x-www-form-urlencoded",
}

func ibcReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"vendor_id": param["vendor_id"].(string),
		"Vendor_Member_ID": param["prefix"].(string) + param["username"].(string),
		"OperatorId": param["operator_id"].(string),
		"UserName": param["prefix"].(string) + param["username"].(string),
		"OddsType": param["odds_type"].(string),
		"Currency": ibcCurrency[param["lang"].(string)],
		"MaxTransfer": param["max_transfer"].(string),
		"MinTransfer": param["min_transfer"].(string),
	}
	requestURI := fmt.Sprintf("%s/api/CreateMember/", param["api"].(string))
	requestBody := paramEncode(args)

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(requestBody), param["username"].(string), IBC, requestURI, ibcHead)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("error_code") == 0  || v.GetInt("error_code") == 6{
		return Success, "success"
	}

	return Failure, string(v.GetStringBytes("message"))
}

//func ibcLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string){
//
//	code,token := ibcToken(zlog,param)
//	if Success != code {
//		return code,token
//	}
//
//	url := param["h5_url"].(string)
//
//	if param["deviceType"].(string) == "1" {
//		url = param["web_url"].(string)
//	}
//
//	loginUrl := fmt.Sprintf("%s?token=%s&lang=%s",url,token,ibcLang[param["lang"].(string)])
//
//	return Success,loginUrl
//}

func ibcLogin(zlog *fluent.Fluent,param map[string]interface{})(int,string) {
	var p fastjson.Parser

	platform := "2"
	if param["deviceType"].(string) == "1" {
		platform = "1"
	}

	args := map[string]string{
		"vendor_id": param["vendor_id"].(string),
		"Vendor_Member_ID": param["prefix"].(string) + param["username"].(string),
		"platform": platform,
	}
	requestURI := fmt.Sprintf("%s/api/GetSabaUrl/", param["api"].(string))
	requestBody := paramEncode(args)

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(requestBody), param["username"].(string), IBC, requestURI, ibcHead)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("error_code") == 0{
		return Success, string(v.GetStringBytes("Data"))
	}

	return Failure, string(v.GetStringBytes("message"))
}

func ibcToken(zlog *fluent.Fluent, param map[string]interface{}) (int, string){
	var p fastjson.Parser

	args := map[string]string{
		"vendor_id": param["vendor_id"].(string),
		"vendor_member_id": param["prefix"].(string) + param["username"].(string),
	}
	requestURI := fmt.Sprintf("%s/api/Login/", param["api"].(string))
	requestBody := paramEncode(args)

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(requestBody), param["username"].(string), IBC, requestURI, ibcHead)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("error_code") == 0 {

		loginURL := fmt.Sprintf("%s&lang=%s",string(v.GetStringBytes("Data")),ibcLang[param["lang"].(string)])
		return Success, loginURL
	}

	return Failure, string(v.GetStringBytes("message"))
}

func ibcBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"vendor_id": param["vendor_id"].(string),
		"vendor_member_ids": param["prefix"].(string) + param["username"].(string),
		"wallet_id": "1",
	}
	requestURI := fmt.Sprintf("%s/api/CheckUserBalance/", param["api"].(string))
	requestBody := paramEncode(args)

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(requestBody), param["username"].(string), IBC, requestURI, ibcHead)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("error_code") == 0 {
		data := v.GetArray("Data")
		if len(data) < 1 {
			return Success, "0.0000"
		}

		return Success, GetBalanceFromFloat(data[0].GetFloat64("balance"))
	}

	return Failure, string(v.GetStringBytes("message"))
}

func ibcTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	direction := "0"
	if param["type"].(string) == "in" {
		direction = "1"
	}

	args := map[string]string{
		"vendor_id": param["vendor_id"].(string),
		"vendor_member_id": param["prefix"].(string) + param["username"].(string),
		"vendor_trans_id": param["id"].(string),
		"amount": param["amount"].(string),
		"currency": ibcCurrency[param["lang"].(string)],
		"direction": direction,
		"wallet_id": "1",
	}
	requestURI := fmt.Sprintf("%s/api/FundTransfer/", param["api"].(string))
	requestBody := paramEncode(args)

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(requestBody), param["username"].(string), IBC, requestURI, ibcHead)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("error_code") != 0 {
		return Failure, string(v.GetStringBytes("message"))
	}

	data := v.Get("Data")

	switch data.GetInt("status") {
	case 0:
		return Success,fmt.Sprintf("%d",data.GetInt("trans_id"))
	case 1:
		return Failure,string(v.GetStringBytes("message"))
	}

	return ibcConfirm(zlog,param)
}


//转账确认
func ibcConfirm(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"vendor_id": param["vendor_id"].(string),
		"vendor_trans_id": param["id"].(string),
		"wallet_id": "1",
	}
	requestURI := fmt.Sprintf("%s/api/CheckFundTransfer/", param["api"].(string))
	requestBody := paramEncode(args)

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(requestBody), param["username"].(string), IBC, requestURI, ibcHead)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("error_code") != 0 {
		return Failure, string(v.GetStringBytes("message"))
	}

	data := v.Get("Data")

	switch data.GetInt("status") {
	case 0:
		return Success,fmt.Sprintf("%d",data.GetInt("trans_id"))
	case 1:
		return Failure,string(v.GetStringBytes("message"))
	}

	return Processing, fmt.Sprintf("%d",data.GetInt("trans_id"))
}