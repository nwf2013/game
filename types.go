package casino

const (
	Failure    int = 0 //失败
	Success    int = 1 //成功
	Pending    int = 3 //待处理
	Processing int = 2 //处理中
)

const tag string = "platform"

type PlatLog struct {
	Requesturl  string `json:"requestURL"`
	Requestbody string `json:"requestBody"`
	Statuscode  int    `json:"statusCode"`
	Body        string `json:"body"`
	Level       string `json:"level"`
	Err         string `json:"err"`
	Name        string `json:"name"`
	Username    string `json:"username"`
}

var routes = map[string]callFunc{
	// ob体育
	"obty_reg":      obtyReg,
	"obty_login":    obtyLogin,
	"obty_balance":  obtyBalance,
	"obty_transfer": obtyTransfer,

	// ob彩票
	"obcp_reg":      obcpReg,
	"obcp_login":    obcpLogin,
	"obcp_balance":  obcpBalance,
	"obcp_transfer": obcpTransfer,

	// ob电竞
	"obdj_reg":      obdjReg,
	"obdj_login":    obdjLogin,
	"obdj_balance":  obdjBalance,
	"obdj_transfer": obdjTransfer,

	// ob真人
	"obzr_reg":      obzrReg,
	"obzr_login":    obzrLogin,
	"obzr_balance":  obzrBalance,
	"obzr_transfer": obzrTransfer,

	// ob棋牌
	"obqp_reg":      obqpReg,
	"obqp_login":    obqpLogin,
	"obqp_balance":  obqpBalance,
	"obqp_transfer": obqpTransfer,

	// ob电游
	"obdy_reg":      obdyReg,
	"obdy_login":    obdyLogin,
	"obdy_balance":  obdyBalance,
	"obdy_transfer": obdyTransfer,

	// ebet
	"ebet_reg":      ebetReg,
	"ebet_login":    ebetLogin,
	"ebet_balance":  ebetBalance,
	"ebet_transfer": ebetTransfer,
	"ebet_confirm":  ebetConfirm,
	"ebet_call":     EbetCallback,

	// 雷火电竞
	"tfdj_reg":      tfdjReg,
	"tfdj_login":    tfdjLogin,
	"tfdj_balance":  tfdjBalance,
	"tfdj_transfer": tfdjTransfer,
	"tfdj_call":     TfdjCallback,

	// 完美真人
	"wmzr_reg":      wmzrReg,
	"wmzr_login":    wmzrLogin,
	"wmzr_balance":  wmzrBalance,
	"wmzr_transfer": wmzrTransfer,

	// tcg彩票
	"tcgcp_reg":      tcgcpReg,
	"tcgcp_login":    tcgcpLogin,
	"tcgcp_balance":  tcgcpBalance,
	"tcgcp_transfer": tcgcpTransfer,

	//双赢彩票
	"sgcp_reg":      sgcpReg,
	"sgcp_login":    sgcpLogin,
	"sgcp_balance":  sgcpBalance,
	"sgcp_transfer": sgcpTransfer,

	//IM棋牌
	"imqp_reg":      imqpReg,
	"imqp_login":    imqpLogin,
	"imqp_balance":  imqpBalance,
	"imqp_transfer": imqpTransfer,

	//PG电子
	"pgdy_reg":      pgdyReg,
	"pgdy_login":    pgdyLogin,
	"pgdy_balance":  pgdyBalance,
	"pgdy_transfer": pgdyTransfer,

	//AG真人/AG捕鱼
	"agzr_reg":      agzrReg,
	"agzr_login":    agzrLogin,
	"agzr_balance":  agzrBalance,
	"agzr_transfer": agzrTransfer,
	"agzr_confirm":  agzrConfirm,

	//BG真人
	"bgzr_reg":      bgzrReg,
	"bgzr_login":    bgzrLogin,
	"bgzr_balance":  bgzrBalance,
	"bgzr_transfer": bgzrTransfer,

	//IM体育
	"imty_reg":      imtyReg,
	"imty_login":    imtyLogin,
	"imty_balance":  imtyBalance,
	"imty_transfer": imtyTransfer,
	"imty_call":     ImtyCallback,

	//IM电竞
	"imdj_reg":      imdjReg,
	"imdj_login":    imdjLogin,
	"imdj_balance":  imdjBalance,
	"imdj_transfer": imdjTransfer,
	"imdj_call":     ImdjCallback,

	//CQ9
	"cq9_reg":      cq9Reg,
	"cq9_login":    cq9Login,
	"cq9_balance":  cq9Balance,
	"cq9_transfer": cq9Transfer,

	//开元棋牌
	"kyqp_reg":      kyqpReg,
	"kyqp_login":    kyqpLogin,
	"kyqp_balance":  kyqpBalance,
	"kyqp_transfer": kyqpTransfer,

	//A5彩票
	"a5cp_reg":      a5cpReg,
	"a5cp_login":    a5cpLogin,
	"a5cp_balance":  a5cpBalance,
	"a5cp_transfer": a5cpTransfer,

	//SBO利记体育
	"sbo_reg":      sboReg,
	"sbo_login":    sboLogin,
	"sbo_balance":  sboBalance,
	"sbo_transfer": sboTransfer,

	//BL博乐棋牌
	"bl_reg":      blReg,
	"bl_login":    blLogin,
	"bl_balance":  blBalance,
	"bl_transfer": blTransfer,

	//FY泛亚电竞
	"fydj_reg":      fydjReg,
	"fydj_login":    fydjLogin,
	"fydj_balance":  fydjBalance,
	"fydj_transfer": fydjTransfer,

	//YB彩票
	"ybcp_reg":      ybcpReg,
	"ybcp_login":    ybcpLogin,
	"ybcp_balance":  ybcpBalance,
	"ybcp_transfer": ybcpTransfer,

	//IBC沙巴体育
	"ibc_reg":      ibcReg,
	"ibc_balance":  ibcBalance,
	"ibc_login":    ibcLogin,
	"ibc_transfer": ibcTransfer,
	"ibc_confirm":  ibcConfirm,

	//CMD体育
	"cmd_reg":      cmdReg,
	"cmd_balance":  cmdBalance,
	"cmd_login":    cmdLogin,
	"cmd_transfer": cmdTransfer,
	"cmd_call":     CmdCallback,

	//DS
	"ds_reg":      dsReg,
	"ds_login":    dsLogin,
	"ds_balance":  dsBalance,
	"ds_transfer": dsTransfer,

	//Play Start Gameing System
	"playstar_reg":      playstarReg,
	"playstar_login":    playstarLogin,
	"playstar_balance":  playstarBalance,
	"playstar_transfer": playstarTransfer,
	"playstar_call":     PlaystarCallback,

	//sg
	"sg_reg":      sgReg,
	"sg_login":    sgLogin,
	"sg_balance":  sgBalance,
	"sg_transfer": sgTransfer,
	"sg_call":     SgCallback,
}
