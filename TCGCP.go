package casino

import (
	"fmt"
	"net/url"
	"strconv"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"github.com/wI2L/jettison"
)

const (
	TCGCP = "TCGCP"
)

var tcgcpLang = map[string]string{
	"cn": "ZH_CN", //简体中文
	"vn": "VI",    //越南语
}

//tcg彩票
func tcgcpPack(args map[string]interface{}, merchantCode, desKey, sha256Key string) string {

	params, _ := jettison.Marshal(args)
	encryptedParams, _ := desEncrypt(params, []byte(desKey))
	param := url.Values{}
	param.Set("merchant_code", merchantCode)
	param.Set("params", encryptedParams)
	param.Set("sign", sha256sum([]byte(encryptedParams+sha256Key)))



	return param.Encode()
}

//tcg彩票注册
func tcgcpReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	args := map[string]interface{}{
		"method":   "cm",
		"username": param["prefix"].(string) + param["username"].(string),
		"password": param["password"].(string),
		"currency": tcgcpLang[param["lang"].(string)],
	}
	header := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}
	requestBody := tcgcpPack(args, param["merchant_code"].(string), param["des_key"].(string), param["sha256_key"].(string))

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(requestBody), param["username"].(string), TCGCP, param["api"].(string), header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("status") == 0 {
		return Success, "success"
	}

	return Failure, string(v.GetStringBytes("error_desc"))
}

//tcg彩票登录
func tcgcpLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	platform := param["platform"].(string)
	gameCode := param["gamecode"].(string)
	if param["deviceType"].(string) == "1" {
		platform = "WEB"
	}

	if gameCode == ""{
		gameCode = "Lobby"
	}
	gameMode := "1"
	if param["tester"].(string) == "1"{
		gameMode = "0"
	}
	defaultSeries := 0.00
	defaultSeries,err := strconv.ParseFloat(param["default_series"].(string),64)
	if err != nil  {
		defaultSeries = 99.0
	}
	series := []map[string]interface{}{}
	serie := map[string]interface{}{
		"game_group_code" : "VIETNAM_LOTTO",
		"default_series" : defaultSeries,
	}
	series = append(series[0:],serie)

	args := map[string]interface{}{
		"method":           "lg",
		"username":         param["prefix"].(string) + param["username"].(string),
		"product_type":     param["product_type"].(string),
		"game_mode":        gameMode, //会员帐户类型（1 =真实，0 =测试）
		"game_code":        gameCode,                  //param["gamecode"].(string),
		"platform":         platform,
		"lottery_bet_mode": param["lottery_bet_mode"].(string), //彩票游戏模式
		"view":             param["view"].(string),
		"language":         tcgcpLang[param["lang"].(string)],
		"ip_address" :      param["ip"].(string),
		"series" :          series,
	}


	//玩家点击返回游戏的跳转地址（可用于返回到平台，仅支持彩票）
	if _, ok := param["backurl"].(string); ok {
		args["back_url"] = param["backurl"].(string)
	}
	header := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}
	requestBody := tcgcpPack(args, param["merchant_code"].(string), param["des_key"].(string), param["sha256_key"].(string))

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(requestBody), param["username"].(string), TCGCP, param["api"].(string), header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("status") == 0 {
		return Success, fmt.Sprintf("%s/%s", param["login_url"].(string), string(v.GetStringBytes("game_url")))
	}

	return Failure, string(v.GetStringBytes("error_desc"))
}

//tcg彩票余额
func tcgcpBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	args := map[string]interface{}{
		"method":       "gb",
		"username":     param["prefix"].(string) + param["username"].(string),
		"product_type": param["product_type"].(string),
	}
	header := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}
	requestBody := tcgcpPack(args, param["merchant_code"].(string), param["des_key"].(string), param["sha256_key"].(string))

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(requestBody), param["username"].(string), TCGCP, param["api"].(string), header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("status") == 0 {
		return Success, GetBalanceFromFloat(v.GetFloat64("balance"))
	}

	res, _ := GetBalanceFromByte(v.GetStringBytes("error_desc"))
	return Failure, res
}

//tcg彩票转账
func tcgcpTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	transType := map[string]string{
		"in":  "1",
		"out": "2",
	}
	args := map[string]interface{}{
		"method":       "ft",
		"username":     param["prefix"].(string) + param["username"].(string),
		"product_type": param["product_type"].(string),
		"fund_type":    transType[param["type"].(string)], //1 =存款，2 =提款
		"amount":       param["amount"].(string),
		"reference_no": param["id"].(string), //我司转账单号
	}
	header := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}
	requestBody := tcgcpPack(args, param["merchant_code"].(string), param["des_key"].(string), param["sha256_key"].(string))

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(requestBody), param["username"].(string), TCGCP, param["api"].(string), header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("status") == 0 {
		return Success, param["id"].(string)
	}

	return Failure, string(v.GetStringBytes("error_desc"))
}

//转账确认
func tcgcpConfirm(zlog *fluent.Fluent, param map[string]interface{}) bool {

	return false
}
