package casino

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"strings"
)

const CMD = "CMD"

var cmdLang = map[string]string{
	"cn":"zh-CN",
	"vn":"vi-VN",
}

var cmdCurrency = map[string]string{
	"cn":"CNY",
	"vn":"VD",
}


// 登录回调
func CmdCallback(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {
	state := 2
	msg := "token null"
	username := ""

	keyB := []byte(param["partner_key"].(string))
	keyByte := make([]byte, 8) //设置加密数组

	tokenParam, ok := param["token"].(string)

	if !ok || tokenParam == "" {
		return Failure, msg
	}

	copy(keyByte[0:], keyB[0:8])

	strByte, _ := hex.DecodeString(tokenParam)

	tokenByte, _ := desDecrypt(strByte, keyByte)
	token := string(tokenByte)

	msg = "token error"
	tokenArr := strings.Split(token, "-")
	if len(tokenArr) == 2 {
		username = tokenArr[1]
	}

	if username != "" {

		msg = "Success"
		state = 0

	}

	res := fmt.Sprintf(`<?xml version="1.0" encoding="UTF-8"?><authenticate><member_id>%s</member_id><status_code>%d</status_code><message>%s</message></authenticate>`,username,state,msg)

	l := PlatLog{
		Requesturl:  "",
		Requestbody: token,
		Statuscode:  state,
		Name:        IMTY,
		Level:       "info",
		Body:        res,
		Err:         "",
	}

	err := zlog.Post(tag, l)
	if err != nil {
		fmt.Printf("Push CMD log is error: %s \n", err.Error())
	}

	return Success, res
}

// 注册
func cmdReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"Method":"createmember",
		"PartnerKey":param["partner_key"].(string),
		"UserName":param["prefix"].(string) + param["username"].(string),
		"Currency":cmdCurrency[param["lang"].(string)],
	}

	paramStr := paramEncode(args)
	requestURI := fmt.Sprintf("%s/SportsApi.aspx?%s", param["api"].(string),paramStr)
	statusCode, body, err := httpGetWithPushLog(zlog, param["username"].(string), CMD, requestURI)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("Code") == 0 || v.GetInt("Code") == -98 {
		return Success, "success"
	}

	return Failure, string(v.GetStringBytes("Message"))
}

// 注册
func cmdLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	username := param["prefix"].(string)+param["username"].(string)
	tokenStr := fmt.Sprintf("CMD_%s-%s", param["ip"].(string), username)

	keyB := []byte(param["partner_key"].(string))
	keyByte := make([]byte, 8) //设置加密数组

	copy(keyByte[0:], keyB[0:8])

	token, _ := desEncrypt([]byte(tokenStr), keyByte)
	tokenB,_ := base64.StdEncoding.DecodeString(token)
	token = hex.EncodeToString(tokenB)

	url := param["h5_url"].(string)

	if param["deviceType"].(string) == "1" {
		url = param["web_url"].(string)
	}

	loginUrl := fmt.Sprintf("%s/auth.aspx?lang=%s&user=%s&token=%s&currency=%s&templatename=%s",url,cmdLang[param["lang"].(string)],username,token,cmdCurrency[param["lang"].(string)],param["templatename"].(string))

	return Success,loginUrl
}

// 查询余额
func cmdBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"Method":"getbalance",
		"PartnerKey":param["partner_key"].(string),
		"UserName":param["prefix"].(string) + param["username"].(string),
	}
	paramStr := paramEncode(args)
	requestURI := fmt.Sprintf("%s/SportsApi.aspx?%s", param["api"].(string),paramStr)

	statusCode, body, err := httpGetWithPushLog(zlog, param["username"].(string), CMD, requestURI)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("Code") != 0 {
		return Failure, string(v.GetStringBytes("Message"))
	}

	data := v.GetArray("Data")
	if len(data) < 1{
		return Success,"0.00"
	}

	return Success, GetBalanceFromFloat(data[0].GetFloat64("BetAmount"))
}

// 转账
func cmdTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	paymentType := "0"
	if param["type"].(string) == "in" {
		paymentType = "1"
	}
	args := map[string]string{
		"Method":"balancetransfer",
		"PartnerKey":param["partner_key"].(string),
		"UserName":param["prefix"].(string) + param["username"].(string),
		"PaymentType":paymentType,
		"Money":param["amount"].(string),
		"TicketNo":param["id"].(string),
	}
	paramStr := paramEncode(args)
	requestURI := fmt.Sprintf("%s/SportsApi.aspx?%s", param["api"].(string), paramStr)

	statusCode, body, err := httpGetWithPushLog(zlog, param["username"].(string), CMD, requestURI)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("Code") == 0 {

		data := v.Get("Data")
		return Success, fmt.Sprintf("%d",data.GetInt64("PaymentId"))
	}

	return Failure, string(v.GetStringBytes("Message"))
}