package casino

import (
	"fmt"
	"net/url"
	"strconv"

	"strings"

	"encoding/xml"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
)

const (
	agCreate   = "lg"
	agBalance  = "gb"
	agTransfer = "tc"
	agConfirm  = "tcc"
	AG         = "AG"
)

var agCurrency = map[string]string{
	"cn": "CNY",
	"vn": "VND",
}

var agLang = map[string]string{
	"cn": "zh-cn",
	"vn": "vi",
}

/**
* AG平台正常返回值对应的xml格式
 */
type agResult struct {
	Info string `xml:"info,attr"`
	Msg  string `xml:"msg,attr"`
}

func agzrPack(md5Key, desKey string, args map[string]string) (string, string) {

	arr := []string{}

	for k, v := range args {
		arr = append(arr[0:], k+"="+v)
	}

	desStr := strings.Join(arr, "/\\\\/")
	paramStr, _ := desEncrypt([]byte(desStr), []byte(desKey))
	key := getMD5Hash(paramStr + md5Key)

	return paramStr, key
}

// 注册
func agzrReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p agResult
	actype := "1"
	if param["tester"].(string) == "1" {
		actype = "0"
	}

	args := map[string]string{
		"cagent":    param["cagent"].(string),
		"loginname": param["prefix"].(string) + param["username"].(string),
		"method":    agCreate,
		"password":  param["password"].(string),
		"actype":    actype,
		"oddtype":   param["oddtype"].(string),
		"cur":       agCurrency[param["lang"].(string)],
	}
	paramStr, keyStr := agzrPack(param["md5_key"].(string), param["des_key"].(string), args)
	requestURI := fmt.Sprintf("%s/doBusiness.do?params=%s&key=%s", param["api"].(string), paramStr, keyStr)

	u, _ := url.Parse(param["api"].(string))
	headers := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
		"Host":         u.Host,
	}
	statusCode, body, err := httpPostWithPushLog(zlog, nil, param["username"].(string), AG, requestURI, headers)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	err = xml.Unmarshal(body, &p)
	if err != nil {
		return Failure, err.Error()
	}

	if p.Info == "0" || strings.Contains(p.Msg, "error:60002,Account password error") {
		return Success, "success"
	}

	return Failure, p.Msg
}

func agzrLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	sid := param["cagent"].(string) + param["ms"].(string)
	gameType := param["gamecode"].(string)
	if param["deviceType"].(string) == "1" && gameType == "" {
		gameType = "0"
	}
	actype := "1"
	if param["tester"].(string) == "1" {
		actype = "0"
	}
	args := map[string]string{
		"cagent":    param["cagent"].(string),
		"loginname": param["prefix"].(string) + param["username"].(string),
		"method":    agCreate,
		"actype":    actype,
		"password":  param["password"].(string),
		"oddtype":   param["oddtype"].(string),
		"dm":        param["backurl"].(string),
		"sid":       sid,
		"lang":      agLang[param["lang"].(string)],
		"gameType":  gameType,
		"cur":       agCurrency[param["lang"].(string)],
	}

	if param["deviceType"].(string) != "1" {
		args["mh5"] = "y"
		if gameType == "" {
			args["gameType"] = "11"
		}
	}

	paramStr, keyStr := agzrPack(param["md5_key"].(string), param["des_key"].(string), args)
	loginUrl := fmt.Sprintf("%s/forwardGame.do?params=%s&key=%s", param["gci"].(string), paramStr, keyStr)

	return Success, loginUrl
}

func agzrBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p agResult

	actype := "1"
	if param["tester"].(string) == "1" {
		actype = "0"
	}
	args := map[string]string{
		"cagent":    param["cagent"].(string),
		"loginname": param["prefix"].(string) + param["username"].(string),
		"method":    agBalance,
		"password":  param["password"].(string),
		"actype":    actype,
		"cur":       agCurrency[param["lang"].(string)],
	}
	paramStr, keyStr := agzrPack(param["md5_key"].(string), param["des_key"].(string), args)
	requestURI := fmt.Sprintf("%s/doBusiness.do?params=%s&key=%s", param["api"].(string), paramStr, keyStr)
	u, _ := url.Parse(param["api"].(string))
	headers := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
		"Host":         u.Host,
	}
	statusCode, body, err := httpPostWithPushLog(zlog, nil, param["username"].(string), AG, requestURI, headers)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	err = xml.Unmarshal(body, &p)
	if err != nil {
		return Failure, err.Error()
	}

	_, err = strconv.ParseFloat(p.Info, 64)

	if err == nil && len(p.Msg) < 1 {
		res, _ := GetBalanceFromString(p.Info)
		return Success, res
	}

	return Failure, p.Info
}

func agzrTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p agResult
	actype := "1"
	if param["tester"].(string) == "1" {
		actype = "0"
	}
	args := map[string]string{
		"cagent":    param["cagent"].(string),
		"loginname": param["prefix"].(string) + param["username"].(string),
		"method":    agTransfer,
		"password":  param["password"].(string),
		"billno":    param["id"].(string),
		"type":      strings.ToUpper(param["type"].(string)),
		"credit":    param["amount"].(string),
		"actype":    actype,
		"cur":       agCurrency[param["lang"].(string)],
	}
	paramStr, keyStr := agzrPack(param["md5_key"].(string), param["des_key"].(string), args)
	requestURI := fmt.Sprintf("%s/doBusiness.do?params=%s&key=%s", param["api"].(string), paramStr, keyStr)

	headers := map[string]string{
		"Content-Type": "Content-type: application/x-www-form-urlencoded\\r\\n",
	}
	statusCode, body, err := httpPostWithPushLog(zlog, nil, param["username"].(string), AG, requestURI, headers)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	err = xml.Unmarshal(body, &p)
	if err != nil {
		return Failure, err.Error()
	}

	if p.Info != "0" {
		return Failure, p.Msg
	}

	param["flag"] = "1"
	if status, _ := agzrConfirm(zlog, param); status != Success {
		return Failure, "转账失败"
	}

	return Success, param["id"].(string)
}

//转账确认
func agzrConfirm(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {
	//确认转账
	var p agResult

	actype := "1"
	if param["tester"].(string) == "1" {
		actype = "0"
	}
	args := map[string]string{
		"cagent":    param["cagent"].(string),
		"loginname": param["prefix"].(string) + param["username"].(string),
		"method":    agConfirm,
		"password":  param["password"].(string),
		"billno":    param["id"].(string),
		"type":      strings.ToUpper(param["type"].(string)),
		"credit":    param["amount"].(string),
		"actype":    actype,
		"cur":       agCurrency[param["lang"].(string)],
		"flag":      param["flag"].(string),
	}
	paramStr, keyStr := agzrPack(param["md5_key"].(string), param["des_key"].(string), args)
	requestURI := fmt.Sprintf("%s/doBusiness.do?params=%s&key=%s", param["api"].(string), paramStr, keyStr)
	headers := map[string]string{
		"Content-Type": "Content-type: application/x-www-form-urlencoded\\r\\n",
	}
	statusCode, body, err := httpPostWithPushLog(zlog, nil, param["username"].(string), AG, requestURI, headers)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, ""
	}

	err = xml.Unmarshal(body, &p)
	if err != nil {
		return Failure, ""
	}

	if p.Info != "0" {
		return Failure, ""
	}

	return Success, ""
}
