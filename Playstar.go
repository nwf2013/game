package casino

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"github.com/wI2L/jettison"
	"strconv"
	"strings"
)

const Playstar string = "PLAYSTAR"

var playstarLang = map[string]string{
	"cn": "zh-CN",
	"vn": "vi-VN",
}

var playstarCurrency = map[string]string{
	"cn": "CNY",
	"vn": "VND",
}

type playstarResponse struct {
	StatusCode int    `json:"status_code"`
	MemberId   string `json:"member_id"`
	MemberName string `json:"member_name"`
}

func playstarPack(args map[string]interface{}) string {

	var data []string
	for k, v := range args {
		data = append(data[0:], fmt.Sprintf("%s=%v", k, v))
	}

	return strings.Join(data, "&")
}

func playstarReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]interface{}{
		"host_id":   param["host_id"].(string),
		"member_id": param["prefix"].(string) + param["username"].(string),
		"purpose":   0,
	}

	str := playstarPack(args)

	reqUrl := fmt.Sprintf("%s/funds/createplayer/?%s", param["api"].(string), str)

	statusCode, body, err := httpGetWithPushLog(zlog, param["username"].(string), Playstar, reqUrl)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("status_code") == 0 || v.GetInt("status_code") == 1 {
		return Success, "success"
	}

	return Failure, string(v.GetStringBytes("msg"))
}

func PlaystarCallback(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	state := 1
	msg := "token null"
	username := ""

	keyB := []byte(param["key"].(string))
	keyByte := make([]byte, 8) //设置加密数组

	tokenParam, ok := param["token"].(string)

	if !ok || tokenParam == "" {
		return Failure, msg
	}

	copy(keyByte[0:], keyB[0:8])

	strByte, _ := hex.DecodeString(tokenParam)

	tokenByte, _ := desDecrypt(strByte, keyByte)
	token := string(tokenByte)

	msg = "token error"
	tokenArr := strings.Split(token, "-")
	if len(tokenArr) == 2 {
		username = tokenArr[1]
	}

	if username != "" {
		msg = "success"
		state = 0
	}

	body := playstarResponse{StatusCode: state, MemberId: username, MemberName: username}
	res, _ := jettison.Marshal(body)

	l := PlatLog{
		Requesturl:  "",
		Requestbody: token,
		Statuscode:  state,
		Name:        Playstar,
		Level:       "info",
		Body:        string(res),
		Err:         "",
	}

	err := zlog.Post(tag, l)
	if err != nil {
		fmt.Printf("Push SG log is error: %s \n", err.Error())
	}

	return Success, string(res)
}

func playstarLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	tokenStr := fmt.Sprintf("Playstar_%s-%s", param["ip"].(string), param["prefix"].(string)+param["username"].(string))

	keyB := []byte(param["key"].(string))
	keyByte := make([]byte, 8) //设置加密数组
	copy(keyByte[0:], keyB[0:8])

	token, _ := desEncrypt([]byte(tokenStr), keyByte)
	tokenB,_ := base64.StdEncoding.DecodeString(token)
	token = hex.EncodeToString(tokenB)
	args := map[string]interface{}{
		"host_id":      param["host_id"].(string),
		"game_id":      param["gamecode"].(string),
		"lang":         playstarLang[param["lang"].(string)],
		"access_token": token,
	}

	body := playstarPack(args)

	return Success, fmt.Sprintf("%s/launch/?%s", param["api"].(string), body)
}

func playstarBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]interface{}{
		"host_id":   param["host_id"].(string),
		"member_id": param["prefix"].(string) + param["username"].(string),
		"purpose":   0,
	}

	str := playstarPack(args)
	reqUrl := fmt.Sprintf("%s/funds/getbalance/?%s", param["api"].(string), str)

	statusCode, body, err := httpGetWithPushLog(zlog, param["username"].(string), Playstar, reqUrl)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("status_code") == 0 {

		balance := float64(v.GetInt("balance")) * 0.01
		return Success, GetBalanceFromFloat(balance)
	}

	return Failure, string(v.GetStringBytes("msg"))
}

func playstarTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	amount, err := strconv.ParseFloat(param["amount"].(string),64)
	if err != nil {
		return Failure, err.Error()
	}

	amount = amount * 100.00
	args := map[string]interface{}{
		"host_id":   param["host_id"].(string),
		"member_id": param["prefix"].(string) + param["username"].(string),
		"txn_id":    param["id"].(string),
		"purpose":   0,
		"amount":    fmt.Sprintf("%.0f",amount),
	}

	method := "deposit"
	if param["type"] == "out" {
		method = "withdraw"
	}

	str := playstarPack(args)
	reqUrl := fmt.Sprintf("%s/funds/%s/?%s", param["api"].(string), method, str)

	statusCode, body, err := httpGetWithPushLog(zlog, param["username"].(string), Playstar, reqUrl)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("status_code") == 0 {
		return Success, param["id"].(string)
	}

	return Failure, string(v.GetStringBytes("msg"))
}

func playstarConfirm(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	return Success, "success"
}
