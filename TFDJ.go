package casino

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"github.com/shopspring/decimal"
	"strings"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"github.com/wI2L/jettison"
)

const (
	TFDJ = "TFDJ"
)

// 登录回调
func TfdjCallback(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	msg := "token null"
	username := ""

	keyB := []byte(param["private_token"].(string))
	keyByte := make([]byte, 8) //设置加密数组

	tokenParam, ok := param["token"].(string)

	if !ok || tokenParam == "" {
		return Failure, msg
	}

	copy(keyByte[0:], keyB[0:8])

	strByte, _ := hex.DecodeString(tokenParam)

	tokenByte, _ := desDecrypt(strByte, keyByte)
	token := string(tokenByte)

	msg = "token error"
	tokenArr := strings.Split(token, "-")
	if len(tokenArr) < 2 {
		return Failure, msg
	}

	username = tokenArr[1]
	return Success, username
}

//雷火电竞注册
func tfdjReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	args := map[string]string{
		"member_code": param["prefix"].(string) + param["username"].(string),
	}

	//场馆限额参数，可选
	if _, ok := param["limit_amount"].(string); ok {
		args["limit_amount_per_event"] = param["limit_amount"].(string)
	}

	requestBody, err := jettison.Marshal(args)
	if err != nil {
		return Failure, err.Error()
	}
	requestURI := fmt.Sprintf("%s/api/v2/members/", param["api"].(string))
	header := map[string]string{
		"Content-Type":  "application/json",
		"Authorization": "Token " + param["private_token"].(string),
	}

	statusCode, _, err := httpPostWithPushLog(zlog, requestBody, param["username"].(string), TFDJ, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusCreated {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	return Success, "success"
}

//雷火电竞登录
func tfdjLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	tokenStr := fmt.Sprintf("TFDJ_%s-%s", param["ip"].(string), param["prefix"].(string)+param["username"].(string))

	keyB := []byte(param["private_token"].(string))
	keyByte := make([]byte, 8) //设置加密数组
	copy(keyByte[0:], keyB[0:8])

	token, _ := desEncrypt([]byte(tokenStr), keyByte)
	tokenB,_ := base64.StdEncoding.DecodeString(token)
	token = hex.EncodeToString(tokenB)

	return Success, fmt.Sprintf("%s/?auth=%s&token=%s", param["login_url"].(string), param["public_token"].(string), token)
}

//雷火电竞余额
func tfdjBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	requestURI := fmt.Sprintf("%s/api/v2/balance/?LoginName=%s", param["api"].(string), param["prefix"].(string)+param["username"].(string))
	header := map[string]string{
		"Authorization": "Token " + param["private_token"].(string),
	}

	statusCode, body, err := httpGetHeaderWithLog(zlog, param["username"].(string), TFDJ, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	vv := v.GetArray("results")

	if vv == nil || len(vv) < 1 {
		return Success, "0.00"
	}

	return Success, GetBalanceFromFloat(vv[0].GetFloat64("balance"))
}

//雷火电竞上下分
func tfdjTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser
	//请求格式
	args := map[string]string{
		"member":       param["prefix"].(string) + param["username"].(string),
		"operator_id":  param["partner_id"].(string),
		"amount":       param["amount"].(string),
		"reference_no": param["id"].(string),
	}

	amount,err := decimal.NewFromString(param["amount"].(string))
	if err != nil {
		fmt.Println("TFDJ amount = ",param["amount"].(string))
		return Failure, err.Error()
	}

	//请求url
	requestURI := ""
	if param["type"].(string) == "in" {
		requestURI = fmt.Sprintf("%s/api/v2/deposit/", param["api"].(string))
	} else {
		requestURI = fmt.Sprintf("%s/api/v2/withdraw/", param["api"].(string))
	}
	header := map[string]string{
		"Content-Type":  "application/json",
		"Authorization": "Token " + param["private_token"].(string),
	}

	requestBody, err := jettison.Marshal(args)
	if err != nil {
		return Failure, err.Error()
	}
	statusCode, body, err := httpPostWithPushLog(zlog, requestBody, param["username"].(string), TFDJ, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusCreated {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	returnAmount := decimal.NewFromFloat(v.GetFloat64("amount"))
	if string(v.GetStringBytes("member")) == args["member"] && string(v.GetStringBytes("reference_no")) == args["reference_no"] && amount.Equal(returnAmount) {
		return Success, param["id"].(string)
	}
	return Failure, "transfer error"
}

//转账确认
func tfdjConfirm(zlog *fluent.Fluent, param map[string]interface{}) bool {

	return false
}
