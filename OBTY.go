package casino

import (
	"fmt"
	"net/url"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
)

const (
	OBTY = "OBTY"
)

func obtyPack(key string, vals ...string) string {

	str := ""
	for _, v := range vals {
		str += v + "&"
	}

	str = str[:len(str)-1]
	sign := getMD5Hash(fmt.Sprintf("%s&%s", getMD5Hash(str), key))

	return sign
}

// 注册
func obtyReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := url.Values{}
	args.Set("userName", param["prefix"].(string)+param["username"].(string))
	args.Set("nickname", param["prefix"].(string)+param["username"].(string))
	args.Set("merchantCode", param["merchant"].(string))
	args.Set("timestamp", param["ms"].(string))
	url := fmt.Sprintf("%s/api/user/create", param["api"].(string))
	sign := obtyPack(param["key"].(string), args.Get("userName"), args.Get("merchantCode"), args.Get("timestamp"))
	args.Set("signature", sign)

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(args.Encode()), param["username"].(string), OBTY, url, nil)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if string(v.GetStringBytes("code")) == "0000" || string(v.GetStringBytes("code")) == "2003" {
		return Success, "success"
	}

	return Failure, string(v.GetStringBytes("msg"))
}

// 登录
func obtyLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := url.Values{}
	args.Set("userName", param["prefix"].(string)+param["username"].(string))
	args.Set("terminal", "pc")
	args.Set("callbackUrl", param["backurl"].(string))
	args.Set("merchantCode", param["merchant"].(string))
	args.Set("timestamp", param["ms"].(string))
	url := fmt.Sprintf("%s/api/user/login", param["api"].(string))
	sign := obtyPack(param["key"].(string), args.Get("merchantCode"), args.Get("userName"), args.Get("terminal"), args.Get("timestamp"))
	args.Set("signature", sign)

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(args.Encode()), param["username"].(string), OBTY, url, nil)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if string(v.GetStringBytes("code")) == "0000" {
		vv := v.Get("data")
		return Success, fmt.Sprintf("%s?token=%s", string(vv.GetStringBytes("domain")), string(vv.GetStringBytes("token")))
	}

	return Failure, string(v.GetStringBytes("msg"))
}

// 获取余额
func obtyBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := url.Values{}
	args.Set("userName", param["prefix"].(string)+param["username"].(string))
	args.Set("merchantCode", param["merchant"].(string))
	args.Set("timestamp", param["ms"].(string))
	url := fmt.Sprintf("%s/api/fund/checkBalance", param["api"].(string))
	sign := obtyPack(param["key"].(string), args.Get("merchantCode"), args.Get("userName"), args.Get("timestamp"))
	args.Set("signature", sign)

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(args.Encode()), param["username"].(string), OBTY, url, nil)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if string(v.GetStringBytes("code")) == "0000" {
		vv := v.Get("data")
		return Success, GetBalanceFromFloat(vv.GetFloat64("balance"))
	}

	return Failure, string(v.GetStringBytes("msg"))
}

// 转账
func obtyTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := url.Values{}
	args.Set("userName", param["prefix"].(string)+param["username"].(string))
	args.Set("amount", param["amount"].(string))
	args.Set("transferId", param["id"].(string))
	args.Set("transferType", "1")
	args.Set("merchantCode", param["merchant"].(string))
	args.Set("timestamp", param["ms"].(string))
	if param["type"].(string) == "out" {
		args.Set("transferType", "2")
	}
	if len(param["id"].(string)) > 19 {
		args.Set("transferId", param["id"].(string)[:19])
	}
	url := fmt.Sprintf("%s/api/fund/transfer", param["api"].(string))
	sign := obtyPack(param["key"].(string), args.Get("merchantCode"), args.Get("userName"), args.Get("transferType"), args.Get("amount"), args.Get("transferId"), args.Get("timestamp"))
	args.Set("signature", sign)

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(args.Encode()), param["username"].(string), OBTY, url, nil)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if string(v.GetStringBytes("code")) == "0000" {
		return Success, param["id"].(string)
	}

	return Failure, string(v.GetStringBytes("msg"))
}

//转账确认
func obtyConfirm(zlog *fluent.Fluent, param map[string]interface{}) bool {

	return false
}
