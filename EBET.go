package casino

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"github.com/wI2L/jettison"
)

type response struct {
	Accesstoken  string `json:"accessToken"`
	Subchannelid int    `json:"subChannelId"`
	Username     string `json:"username"`
	Status       int    `json:"status"`
	Nickname     string `json:"nickname"`
	Currency     string `json:"currency"`
}

const EBET string = "EBET"

var ebetCurrency = map[string]string{
	"cn":"CNY",
	"vn":"VN2",
}

var ebetLang = map[string]string{
	"cn":"zh_cn",
	"vn":"vi_vn",
}

func ebetPack(key, signStr string, args map[string]string) string {

	data := map[string]string{}

	md5Str := getMD5Hash(signStr)
	md5Byte, _ := hex.DecodeString(md5Str)
	signature := rsaEncrypt([]byte(key), md5Byte)
	for k, v := range args {
		data[k] = v
	}
	data["signature"] = base64.StdEncoding.EncodeToString(signature)
	jsonStr, _ := jettison.Marshal(data)

	return string(jsonStr)
}

// 登录回调
func EbetCallback(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	state := 100

	timestamp, ok := param["timestamp"].(int)
	if !ok {
		timestamp = 0
	}

	accessToken, ok := param["accessToken"].(string)
	if !ok {
		accessToken = ""
	}

	checkSignature, ok := param["signature"].(string)
	if !ok {
		checkSignature = ""
	}

	username, ok := param["username"].(string)
	if !ok {
		username = ""
	}

	username = param["prefix"].(string) + username

	currency, ok := ebetCurrency[param["lang"].(string)]
	if !ok {
		currency = ""
	}

	eventType, ok := param["eventType"].(int)
	if !ok {
		eventType = 0
	}

	//判断请求是否合法
	signStr := fmt.Sprintf("%d", timestamp) + accessToken
	md5Str := getMD5Hash(signStr)
	md5Byte, _ := hex.DecodeString(md5Str)
	signature := rsaEncrypt([]byte(param["private_key"].(string)), md5Byte)
	if base64.StdEncoding.EncodeToString(signature) != checkSignature {
		eventType = 0
	}

	switch eventType {
	case 1:
	case 3:
	case 4:
		md5S := getMD5Hash(username + username)
		md5B, _ := hex.DecodeString(md5S)
		rsaStr := rsaEncrypt([]byte(param["private_key"].(string)), md5B)
		accessToken := getMD5Hash(base64.StdEncoding.EncodeToString(rsaStr))
		if accessToken == accessToken {
			state = 200
		}
	}

	res := response{
		Accesstoken:  accessToken,
		Subchannelid: 0,
		Username:     username,
		Status:       state,
		Currency:     currency,
		Nickname:     username,
	}

	jsonStr, _ := jettison.Marshal(param)

	b, _ := jettison.Marshal(res)

	l := PlatLog{
		Requesturl:  "",
		Requestbody: string(jsonStr),
		Statuscode:  state,
		Name:        EBET,
		Level:       "info",
		Body:        string(b),
		Err:         "",
	}

	err := zlog.Post(tag, l)
	if err != nil {
		fmt.Printf("Push EBET log is error: %s \n", err.Error())
	}

	return Success, string(b)
}

// 注册
func ebetReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"channelId":    param["channel_id"].(string),
		"username":     param["prefix"].(string) + param["username"].(string),
		"subChannelId": "0",
		"currency":     ebetCurrency[param["lang"].(string)],
	}

	str := ebetPack(param["private_key"].(string), param["prefix"].(string)+param["username"].(string), args)
	requestURI := fmt.Sprintf("%s/api/syncuser", param["api"].(string))
	headers := map[string]string{
		"Content-Type": "application/json",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), param["username"].(string), EBET, requestURI, headers)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("status") == 200 || v.GetInt("status") == 401 {
		return Success, "success"
	}

	return Failure, fmt.Sprintf("%d", v.GetInt("status"))
}

func ebetLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	signStr := param["prefix"].(string) + param["username"].(string) + param["prefix"].(string) + param["username"].(string)
	md5Str := getMD5Hash(signStr)
	md5Byte, _ := hex.DecodeString(md5Str)
	signature := rsaEncrypt([]byte(param["private_key"].(string)), md5Byte)
	accessToken := getMD5Hash(base64.StdEncoding.EncodeToString(signature))
	loginUrl := fmt.Sprintf("%s?username=%s&accessToken=%s&language=%s", param["h5"].(string), param["prefix"].(string)+param["username"].(string), accessToken,ebetLang[param["lang"].(string)])

	return Success, loginUrl
}

func ebetBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	timestamp := param["ms"].(string)
	args := map[string]string{
		"channelId": param["channel_id"].(string),
		"username":  param["prefix"].(string) + param["username"].(string),
		"currency":  ebetCurrency[param["lang"].(string)],
		"timestamp": timestamp,
	}

	signStr := param["prefix"].(string) + param["username"].(string) + timestamp
	str := ebetPack(param["private_key"].(string), signStr, args)

	requestURI := fmt.Sprintf("%s/api/userinfo", param["api"].(string))
	headers := map[string]string{
		"Content-Type": "application/json",
	}
	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), param["username"].(string), EBET, requestURI, headers)

	if err != nil {
		return Failure, err.Error()
	}

	if err != nil {
		fmt.Printf("Push EBET log is error: %s \n", err.Error())
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}
	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("status") == 200 {
		return Success, GetBalanceFromFloat(v.GetFloat64("money"))
	}

	return Failure, GetBalanceFromFloat(v.GetFloat64("money"))
}

func ebetTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	timestamp := param["ms"].(string)

	amount := param["amount"].(string)
	if param["type"].(string) == "out" {
		amount = "-" + param["amount"].(string)
	}
	args := map[string]string{
		"channelId":     param["channel_id"].(string),
		"username":      param["prefix"].(string) + param["username"].(string),
		"rechargeReqId": param["id"].(string),
		"timestamp":     timestamp,
		"money":         amount,
	}

	signStr := param["prefix"].(string) + param["username"].(string) + timestamp
	str := ebetPack(param["private_key"].(string), signStr, args)
	requestURI := fmt.Sprintf("%s/api/recharge", param["api"].(string))
	headers := map[string]string{
		"Content-Type": "application/json",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), param["username"].(string), EBET, requestURI, headers)
	if err != nil {
		return Processing, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Processing, param["id"].(string)
	}

	v, err := p.ParseBytes(body)

	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("status") == 200 {
		return ebetConfirm(zlog, param)
	}
	return Failure, "failed"
}

//转账确认
func ebetConfirm(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"channelId":     param["channel_id"].(string),
		"currency":      ebetCurrency[param["lang"].(string)],
		"rechargeReqId": param["id"].(string),
	}

	str := ebetPack(param["private_key"].(string), param["id"].(string), args)
	requestURI := fmt.Sprintf("%s/api/rechargestatus", param["api"].(string))
	headers := map[string]string{
		"Content-Type": "application/json",
	}
	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), param["username"].(string), EBET, requestURI, headers)
	if err != nil {
		return Processing, param["id"].(string)
	}

	if statusCode != fasthttp.StatusOK {
		return Processing, param["id"].(string)
	}

	v, err := p.ParseBytes(body)

	if err != nil {
		return Processing, param["id"].(string)
	}

	if v.GetInt("status") == 200 {
		return Success, param["id"].(string)
	}

	if v.GetInt("status") == 0 || v.GetInt("status") == -1{
		return Processing, param["id"].(string)
	}
	return Failure, "failed"
}
