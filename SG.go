package casino

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"github.com/wI2L/jettison"
	"strings"
)

const SG string = "SG"

var sgLang = map[string]string{
	"cn": "zh_CN",
	"vn": "vi_VN",
}

var sgCurrency = map[string]string{
	"cn": "CNY",
	"vn": "VND",
}

type sgAcctInfo struct {
	AcctId   string  `json:"acctId"`
	UserName string  `json:"userName"`
	Currency string  `json:"currency"`
	Balance  float64 `json:"balance"`
}

type sgResponse struct {
	MerchantCode string     `json:"merchantCode"`
	Msg          string     `json:"msg"`
	Code         string     `json:"code"`
	SerialNo     string     `json:"serialNo"`
	AcctInfo     sgAcctInfo `json:"acctInfo"`
}

func sgPack(args map[string]string) (string, error) {

	body, err := jettison.Marshal(args)
	return string(body), err
}

func SgCallback(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	state := 113
	msg := "token null"
	username := ""

	keyB := []byte(param["key"].(string))
	keyByte := make([]byte, 8) //设置加密数组

	tokenParam, ok := param["token"].(string)

	if !ok || tokenParam == "" {
		return Failure, msg
	}

	copy(keyByte[0:], keyB[0:8])

	strByte, _ := hex.DecodeString(tokenParam)

	tokenByte, _ := desDecrypt(strByte, keyByte)
	token := string(tokenByte)

	msg = "token error"
	tokenArr := strings.Split(token, "-")
	if len(tokenArr) == 2 {
		username = tokenArr[1]
	}

	if username != "" {
		msg = "success"
		state = 0
	}

	body := sgResponse{
		MerchantCode: param["merchant_code"].(string),
		Msg:          msg,
		Code:         fmt.Sprintf("%d", state),
		SerialNo:     param["serialNo"].(string),
		AcctInfo: sgAcctInfo{
			AcctId:   username,
			UserName: username,
			Currency: sgCurrency[param["lang"].(string)],
			Balance:  0,
		},
	}

	res, _ := jettison.Marshal(body)

	l := PlatLog{
		Requesturl:  "",
		Requestbody: token,
		Statuscode:  state,
		Name:        SG,
		Level:       "info",
		Body:        string(res),
		Err:         "",
	}

	err := zlog.Post(tag, l)
	if err != nil {
		fmt.Printf("Push SG log is error: %s \n", err.Error())
	}

	return Success, string(res)
}

func sgReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	//var p fastjson.Parser

	tokenStr := fmt.Sprintf("SG_%s-%s", param["ip"].(string), param["prefix"].(string)+param["username"].(string))

	keyB := []byte(param["key"].(string))
	keyByte := make([]byte, 8) //设置加密数组

	copy(keyByte[0:], keyB[0:8])

	token, _ := desEncrypt([]byte(tokenStr), keyByte)
	tokenB,_ := base64.StdEncoding.DecodeString(token)
	token = hex.EncodeToString(tokenB)

	gameplat := "true"
	if param["deviceType"].(string) == "1" {
		gameplat = "false"
	}
	tester := "false"
	gameCode,ok := param["gamecode"].(string)
	if !ok || gameCode == "" {
		gameCode = "S-DG02"
	}

	args := map[string]string{
		"acctId":   param["prefix"].(string) + param["username"].(string),
		"language": sgLang[param["lang"].(string)],
		"token":    token,
		"lobby":    "SG",
		"mobile":   gameplat,
		"game":     gameCode,
		"fun":      tester,
	}

	var params []string
	for k, v := range args {
		params = append(params, fmt.Sprintf("%s=%s", k, v))
	}

	url := fmt.Sprintf("%s/KOKVN/auth/?%s", param["lobby"].(string), strings.Join(params, "&"))

	statusCode, body, err := httpGetWithPushLog(zlog, param["username"].(string), SG, url)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	fmt.Printf(string(body))

	return Success, "success"
}

func sgLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	tokenStr := fmt.Sprintf("SG_%s-%s", param["ip"].(string), param["prefix"].(string)+param["username"].(string))

	keyB := []byte(param["key"].(string))
	keyByte := make([]byte, 8) //设置加密数组

	copy(keyByte[0:], keyB[0:8])

	token, _ := desEncrypt([]byte(tokenStr), keyByte)
	tokenB,_ := base64.StdEncoding.DecodeString(token)
	token = hex.EncodeToString(tokenB)

	gameplat := "true"
	if param["deviceType"].(string) == "1" {
		gameplat = "false"
	}
	tester := "false"
	gameCode,ok := param["gamecode"].(string)
	if !ok || gameCode == "" {
		gameCode = "S-DG02"
	}
	args := map[string]string{
		"acctId":   param["prefix"].(string) + param["username"].(string),
		"language": sgLang[param["lang"].(string)],
		"token":    token,
		"lobby":    "SG",
		"mobile":   gameplat,
		"game":     gameCode,
		"fun":      tester,
	}

	var params []string
	for k, v := range args {
		params = append(params, fmt.Sprintf("%s=%s", k, v))
	}

	return Success, fmt.Sprintf("%s/KOKVN/auth/?%s", param["lobby"].(string), strings.Join(params, "&"))
}

func sgBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"serialNo":     param["id"].(string),
		"merchantCode": param["merchant_code"].(string),
		"acctId":       param["prefix"].(string) + param["username"].(string),
		"pageIndex":    "1",
	}

	str, err := sgPack(args)
	if err != nil {
		return Failure, err.Error()
	}
	fmt.Println(str)
	reqUrl := fmt.Sprintf("%s/api/createplayer", param["api"].(string))
	header := map[string]string{
		"DataType": "JSON",
		"API":      "getAcctInfo",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), param["username"].(string), SG, reqUrl, header)
	fmt.Println(string(body))
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("code") == 0 {

		vv := v.Get("list")
		vals, err := vv.Array()
		if len(vals) > 0 {
			if err != nil {
				return Failure, string(v.GetStringBytes("msg"))
			}

			return Success, GetBalanceFromFloat(vals[0].GetFloat64("balance"))
		}
		return Success, "0.0000"
	}

	return Failure, string(v.GetStringBytes("msg"))
}

func sgTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"serialNo":     param["id"].(string),
		"merchantCode": param["merchant_code"].(string),
		"acctId":       param["prefix"].(string) + param["username"].(string),
		"currency":     sgCurrency[param["lang"].(string)],
		"amount":       param["amount"].(string),
	}

	str, err := sgPack(args)
	if err != nil {
		return Failure, err.Error()
	}
	fmt.Println(str)
	method := "deposit"
	if param["type"] == "out" {
		method = "withdraw"
	}

	reqUrl := fmt.Sprintf("%s/api/%s", param["api"].(string), method)
	header := map[string]string{
		"DataType": "JSON",
		"API":      method,
	}

	fmt.Println(reqUrl)
	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), param["username"].(string), SG, reqUrl, header)
	fmt.Println(string(body))
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	if v.GetInt("code") == 0 {
		return Success, string(v.GetStringBytes("transactionId"))
	}

	return Failure, string(v.GetStringBytes("msg"))
}

func sgConfirm(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	return Success, "success"
}
