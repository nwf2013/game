package casino

import (
	b64 "encoding/base64"
	"fmt"
	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"github.com/wI2L/jettison"
)

const A5CP string = "A5CP"

var a5cpLang = map[string]string{
	"cn": "zh_cn",
	"vn": "vi_vn",
}

var a5cpCurrency = map[string]string{
	"cn": "RMB",
	"vn": "VND2",
}

func a5cpPack(aesKey, method, lang, id string, args map[string]string) string {

	text, _ := jettison.Marshal(args)

	encrypted := aesEcbEncrypt(text, []byte(aesKey))
	sEnc := b64.StdEncoding.EncodeToString(encrypted)

	result := map[string]string{
		"Request": sEnc,
		"Method":  method,
		"AGID":    id,
		"lang":    a5cpLang[lang],
	}

	s, _ := jettison.Marshal(result)

	return string(s)
}

func a5cpToken(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"username": param["agname"].(string),
		"password": param["pwd"].(string),
		"remoteip": param["ip"].(string),
	}

	str := a5cpPack(param["key"].(string), "AGLogin", a5cpLang[param["lang"].(string)], param["agid"].(string), args)
	header := map[string]string{
		"Content-Type": "application/json;charset=utf-8",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), param["username"].(string),A5CP, param["url"].(string), header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	bytesPass, err := b64.StdEncoding.DecodeString(string(body))

	if err != nil {
		return Failure, err.Error()
	}

	result, _ := aesEcbDecrypt(bytesPass, []byte(param["key"].(string)))

	v, err := p.ParseBytes(result)
	if err != nil {
		return Failure, err.Error()
	}

	if string(v.GetStringBytes("respcode")) == "0000" {
		return Success, string(v.GetStringBytes("token"))
	}

	return Failure, string(v.GetStringBytes("errormessage"))
}

func a5cpReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	code, token := a5cpToken(zlog, param)
	if code == Failure {
		return code, token
	}

	args := map[string]string{
		"memname":               param["prefix"].(string) + param["username"].(string),
		"BoundsLimitTemplateID": "0",
		"LimitTemplateID":       "0",
		"OddsTemplateID":        "0",
		"remoteip":              param["ip"].(string),
		"token":                 token,
		"currency":              a5cpCurrency[param["lang"].(string)],
	}

	str := a5cpPack(param["key"].(string), "CreateMember", a5cpLang[param["lang"].(string)], param["agid"].(string), args)
	header := map[string]string{
		"Content-Type": "application/json;charset=utf-8",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), param["username"].(string), A5CP, param["url"].(string), header)

	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	bytesPass, err := b64.StdEncoding.DecodeString(string(body))

	if err != nil {
		return Failure, err.Error()
	}

	result, _ := aesEcbDecrypt(bytesPass, []byte(param["key"].(string)))

	v, err := p.ParseBytes(result)
	if err != nil {
		return Failure, err.Error()
	}

	if string(v.GetStringBytes("respcode")) == "0000" {
		return Success, "success"
	}

	return Failure, string(v.GetStringBytes("errormessage"))
}

func a5cpLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	code, msg := a5cpToken(zlog, param)
	if code == Failure {
		return code, msg
	}

	gameplat := "MOBILE"
	if param["deviceType"].(string) == "1" {
		gameplat = "PC"
	}

	gameID := param["gamecode"].(string)

	if gameID == "" {
		gameID = "Lobby"
	}

	lang := param["lang"].(string)
	args := map[string]string{
		"memname":  param["prefix"].(string) + param["username"].(string),
		"device":   gameplat,
		"lang":     a5cpLang[lang],
		"gameid":   gameID,
		"token":    msg,
		"remoteip": param["ip"].(string),
	}
	str := a5cpPack(param["key"].(string), "LaunchGame", lang, param["agid"].(string), args)
	header := map[string]string{
		"Content-Type": "application/json;charset=utf-8",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), param["username"].(string), A5CP, param["url"].(string), header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	bytesPass, err := b64.StdEncoding.DecodeString(string(body))

	if err != nil {
		return Failure, err.Error()
	}

	result, _ := aesEcbDecrypt(bytesPass, []byte(param["key"].(string)))

	v, err := p.ParseBytes(result)
	if err != nil {
		return Failure, err.Error()
	}

	if string(v.GetStringBytes("respcode")) == "0000" {
		return Success, string(v.GetStringBytes("launchgameurl"))
	}

	return Failure, string(v.GetStringBytes("errormessage"))
}

func a5cpBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	code, msg := a5cpToken(zlog, param)
	if code == Failure {
		return code, msg
	}

	args := map[string]string{
		"memname":  param["prefix"].(string) + param["username"].(string),
		"remoteip": param["ip"].(string),
		"token":    msg,
	}
	str := a5cpPack(param["key"].(string), "ChkMemberBalance", a5cpLang[param["lang"].(string)], param["agid"].(string), args)
	header := map[string]string{
		"Content-Type": "application/json;charset=utf-8",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), param["username"].(string), A5CP, param["url"].(string), header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	bytesPass, err := b64.StdEncoding.DecodeString(string(body))

	if err != nil {
		return Failure, err.Error()
	}

	result, _ := aesEcbDecrypt(bytesPass, []byte(param["key"].(string)))

	v, err := p.ParseBytes(result)
	if err != nil {
		return Failure, err.Error()
	}

	if string(v.GetStringBytes("respcode")) == "0000" {

		amount, _ := GetBalanceFromByte(v.GetStringBytes("balance"))
		return Success, amount
	}

	return Failure, string(v.GetStringBytes("errormessage"))
}

func a5cpTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	orderId := fmt.Sprintf("%s:%s", param["prefix"].(string)+param["username"].(string), param["id"].(string))

	code, msg := a5cpToken(zlog, param)
	if code == Failure {
		return code, msg
	}

	args := map[string]string{
		"memname":  param["prefix"].(string) + param["username"].(string),
		"amount":   param["amount"].(string),
		"remoteip": param["ip"].(string),
		"payno":    orderId,
		"token":    msg,
	}

	method := "Deposit"
	if param["type"] == "out" {
		method = "Withdraw"
	}
	str := a5cpPack(param["key"].(string), method, a5cpLang[param["lang"].(string)], param["agid"].(string), args)
	header := map[string]string{
		"Content-Type": "application/json;charset=utf-8",
	}

	statusCode, body, err := httpPostWithPushLog(zlog, []byte(str), param["username"].(string), A5CP, param["url"].(string), header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	bytesPass, err := b64.StdEncoding.DecodeString(string(body))

	if err != nil {
		return Failure, err.Error()
	}

	result, _ := aesEcbDecrypt(bytesPass, []byte(param["key"].(string)))

	v, err := p.ParseBytes(result)
	if err != nil {
		return Failure, err.Error()
	}

	if string(v.GetStringBytes("respcode")) == "0000" {

		return Success, orderId
	}

	return Failure, string(v.GetStringBytes("errormessage"))
}

func a5cpConfirm(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	return Success, "success"
}
