package casino

import (
	"fmt"
	"net/url"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"github.com/wI2L/jettison"
)

const SBO string = "SBO"

var sboLang = map[string]string{
	"cn": "zh-cn",
	"vn": "vi-vn",
}

// 注册
func sboReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"CompanyKey": param["company_key"].(string),
		"Username":   param["prefix"].(string) + param["username"].(string),
		"ServerId":   param["id"].(string),
		"Agent":      param["agent"].(string),
	}

	jsonStr, _ := jettison.Marshal(args)
	requestURI := fmt.Sprintf("%s/web-root/restricted/player/register-player.aspx", param["api"].(string))
	u, _ := url.Parse(param["api"].(string))
	headers := map[string]string{
		"Content-Type": "application/json",
		"Host":         u.Host,
	}

	statusCode, body, err := httpPostWithPushLog(zlog, jsonStr, param["username"].(string), SBO, requestURI, headers)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	errObject := v.Get("error")
	errId := errObject.GetInt("id")
	if errId == 0 || errId == 4103 {
		return Success, "success"
	}

	return Failure, string(errObject.GetStringBytes("msg"))
}

//这个是代理注册
func sboAgentReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"CompanyKey":       param["company_key"].(string),
		"Username":         "ZF106kokvn001",
		"ServerId":         param["id"].(string),
		"Password":         "a123456789",
		"Currency":         "VND",
		"Min":              "20000",
		"Max":              "20000000",
		"MaxPerMatch":      "80000000",
		"CasinoTableLimit": "3",
	}

	jsonStr, _ := jettison.Marshal(args)
	requestURI := fmt.Sprintf("%s/web-root/restricted/agent/register-agent.aspx", param["api"].(string))
	u, _ := url.Parse(param["api"].(string))
	headers := map[string]string{
		"Content-Type": "application/json",
		"Host":         u.Host,
	}

	statusCode, body, err := httpPostWithPushLog(zlog, jsonStr, param["username"].(string), SBO, requestURI, headers)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	errObject := v.Get("error")
	errId := errObject.GetInt("id")
	if errId == 0 {
		return Success, "success"
	}

	return Failure, string(errObject.GetStringBytes("msg"))
}

func sboLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"CompanyKey": param["company_key"].(string),
		"Username":   param["prefix"].(string) + param["username"].(string),
		"ServerId":   param["id"].(string),
		"Portfolio":  "SportsBook",
	}

	jsonStr, _ := jettison.Marshal(args)
	requestURI := fmt.Sprintf("%s/web-root/restricted/player/login.aspx", param["api"].(string))
	u, _ := url.Parse(param["api"].(string))
	headers := map[string]string{
		"Content-Type": "application/json",
		"Host":         u.Host,
	}

	statusCode, body, err := httpPostWithPushLog(zlog, jsonStr, param["username"].(string), SBO, requestURI, headers)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)

	errObject := v.Get("error")

	errId := errObject.GetInt("id")
	if errId == 0 {
		device := "m"

		if param["deviceType"].(string) == "1" {
			device = "d"
		}
		loginUrl := fmt.Sprintf("https:%s&lang=%s&oddstyle=%s&theme=%s&oddsmode=%s&device=%s", string(v.GetStringBytes("url")), sboLang[param["lang"].(string)], param["oddstyle"].(string), param["theme"].(string), param["oddsmode"].(string), device)
		return Success, loginUrl
	}

	return Failure, string(errObject.GetStringBytes("msg"))
}

func sboBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"CompanyKey": param["company_key"].(string),
		"Username":   param["prefix"].(string) + param["username"].(string),
		"ServerId":   param["id"].(string),
	}

	jsonStr, _ := jettison.Marshal(args)
	requestURI := fmt.Sprintf("%s/web-root/restricted/player/get-player-balance.aspx", param["api"].(string))
	u, _ := url.Parse(param["api"].(string))
	headers := map[string]string{
		"Content-Type": "application/json",
		"Host":         u.Host,
	}

	statusCode, body, err := httpPostWithPushLog(zlog, jsonStr, param["username"].(string), SBO, requestURI, headers)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	errObject := v.Get("error")
	errId := errObject.GetInt("id")
	if errId == 0 {
		balance := v.GetFloat64("balance") - v.GetFloat64("outstanding")
		return Success, GetBalanceFromFloat(balance)
	}

	return Failure, string(errObject.GetStringBytes("msg"))
}

func sboTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]interface{}{
		"CompanyKey": param["company_key"],
		"Username":   param["prefix"].(string) + param["username"].(string),
		"ServerId":   param["id"],
		"Amount":     param["amount"],
		"TxnId":      param["id"],
	}
	requestURI := fmt.Sprintf("%s/web-root/restricted/player/deposit.aspx", param["api"].(string))
	if param["type"].(string) == "out" {
		requestURI = fmt.Sprintf("%s/web-root/restricted/player/withdraw.aspx", param["api"].(string))
		args["IsFullAmount"] = false
	}

	jsonStr, _ := jettison.Marshal(args)
	u, _ := url.Parse(param["api"].(string))
	headers := map[string]string{
		"Content-Type": "application/json",
		"Host":         u.Host,
	}

	statusCode, body, err := httpPostWithPushLog(zlog, jsonStr, param["username"].(string), SBO, requestURI, headers)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	errObject := v.Get("error")
	errId := errObject.GetInt("id")
	if errId == 0 {
		return Success, string(v.GetStringBytes("refno"))
	}

	return Failure, string(errObject.GetStringBytes("msg"))
}
