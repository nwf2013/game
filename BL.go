package casino

import (
	"fmt"

	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"

	"net/url"
)

const BL string = "BL"

var blLang = map[string]string{
	"cn": "zh_CN",
	"vn": "vi_VN",
}

func blPack(md5Key string, args map[string]string) string {

	if len(args) < 1 {
		return ""
	}

	param := url.Values{}
	for k, v := range args {
		param.Set(k, v)
	}

	md5Str := fmt.Sprintf("%s%s%s", md5Key, args["Nonce"], args["Timestamp"])
	param.Set("Sign", fmt.Sprintf("%x", Sha1Sum(md5Str)))

	return param.Encode()
}

// 注册
func blReg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	return blLogin(zlog, param)
}

func blLogin(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"player_account": param["prefix"].(string) + param["username"].(string),
		"country":        "zh",
		"lang":           blLang[param["lang"].(string)],
		"ip":             param["ip"].(string),
		"AccessKeyId":    param["key_id"].(string),
		"Timestamp":      param["s"].(string),
		"Nonce":          randomString(20),
	}

	paramStr := blPack(param["key_secret"].(string), args)
	requestURI := fmt.Sprintf("%s/v1/player/login", param["api"].(string))
	headers := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
	}
	statusCode, body, err := httpPostWithPushLog(zlog, []byte(paramStr), param["username"].(string), BL, requestURI, headers)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	respMsg := v.Get("resp_msg")

	if respMsg.GetInt("code") == 200 {
		respData := v.Get("resp_data")
		return Success, string(respData.GetStringBytes("url"))
	}

	return Failure, string(respMsg.GetStringBytes("message"))
}

func blBalance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"player_account": param["prefix"].(string) + param["username"].(string),
		"AccessKeyId":    param["key_id"].(string),
		"Timestamp":      param["s"].(string),
		"Nonce":          randomString(20),
	}
	paramStr := blPack(param["key_secret"].(string), args)
	requestURI := fmt.Sprintf("%s/v1/player/get_info", param["api"].(string))
	headers := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
	}
	statusCode, body, err := httpPostWithPushLog(zlog, []byte(paramStr), param["username"].(string), BL, requestURI, headers)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	respMsg := v.Get("resp_msg")

	if respMsg.GetInt("code") == 200 {
		respData := v.Get("resp_data")
		return Success, GetBalanceFromFloat(respData.GetFloat64("gold"))
	}

	return Failure, string(respMsg.GetStringBytes("message"))
}

func blTransfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"player_account":    param["prefix"].(string) + param["username"].(string),
		"operator_order_id": param["id"].(string),
		"amount":            param["amount"].(string),
		"AccessKeyId":       param["key_id"].(string),
		"Timestamp":         param["s"].(string),
		"Nonce":             randomString(20),
	}
	paramStr := blPack(param["key_secret"].(string), args)
	uri := "/v1/order/coin_in"

	if param["type"].(string) == "out" {
		uri = "/v1/order/coin_out"
	}
	requestURI := fmt.Sprintf("%s%s", param["api"].(string), uri)
	headers := map[string]string{
		"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
	}
	statusCode, body, err := httpPostWithPushLog(zlog, []byte(paramStr), param["username"].(string), BL, requestURI, headers)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	respMsg := v.Get("resp_msg")

	if respMsg.GetInt("code") == 200 {
		return Success, param["id"].(string)
	}

	return Failure, string(respMsg.GetStringBytes("message"))
}
