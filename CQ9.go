package casino

import (
	"fmt"
	"github.com/fluent/fluent-logger-golang/fluent"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
)

const CQ9 string = "CQ9"

var cq9Lang = map[string]string{
	"cn": "zh-cn", //简体中文
	"vn": "vn",    //越南
}

func cq9Pack(apiToken string, args map[string]string) (string, map[string]string) {

	header := map[string]string{
		"Content-Type":  "application/x-www-form-urlencoded",
		"Authorization": apiToken,
	}

	str := paramEncode(args)

	return str, header
}

// 注册
func cq9Reg(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"account":  param["prefix"].(string) + param["username"].(string),
		"password": param["password"].(string),
	}

	requestURI := fmt.Sprintf("%s/gameboy/player", param["api"].(string))
	requestBody, header := cq9Pack(param["api_token"].(string), args)
	statusCode, body, err := httpPostWithPushLog(zlog, []byte(requestBody), param["username"].(string), CQ9, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	status := v.Get("status")
	if string(status.GetStringBytes("code")) == "0" || string(status.GetStringBytes("code")) == "6" {
		return Success, "success"
	}

	return Failure, string(status.GetStringBytes("message"))
}

// 登陆
func cq9Login(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	//第一步，获取token
	code, token := cq9Token(zlog, param)
	if Success != code {
		return code, token
	}

	var p fastjson.Parser

	gameplat := "mobile"
	if param["deviceType"].(string) == "1" {
		gameplat = "web"
	}
	args := map[string]string{
		"usertoken": token,
		"gamehall":  param["gamehall"].(string),
		"gamecode":  param["gamecode"].(string),
		"gameplat":  gameplat,
		"lang":      cq9Lang[param["lang"].(string)],
	}
	requestURI := fmt.Sprintf("%s/gameboy/player/gamelink", param["api"].(string))
	requestBody, header := cq9Pack(param["api_token"].(string), args)
	statusCode, body, err := httpPostWithPushLog(zlog, []byte(requestBody), param["username"].(string), CQ9, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	status := v.Get("status")
	if string(status.GetStringBytes("code")) != "0" && string(status.GetStringBytes("code")) != "6" {
		return Failure, string(status.GetStringBytes("message"))
	}

	data := v.Get("data")

	//第二步 登录
	return Success, string(data.GetStringBytes("url"))
}

func cq9Token(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {
	var p fastjson.Parser

	args := map[string]string{
		"account":  param["prefix"].(string) + param["username"].(string),
		"password": param["password"].(string),
	}
	requestURI := fmt.Sprintf("%s/gameboy/player/login", param["api"].(string))
	requestBody, header := cq9Pack(param["api_token"].(string), args)
	statusCode, body, err := httpPostWithPushLog(zlog, []byte(requestBody), param["username"].(string), CQ9, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	status := v.Get("status")
	if string(status.GetStringBytes("code")) != "0" {
		return Failure, string(status.GetStringBytes("message"))
	}

	data := v.Get("data")
	return Success, string(data.GetStringBytes("usertoken"))
}

// 查询余额
func cq9Balance(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	requestURI := fmt.Sprintf("%s/gameboy/player/balance/%s", param["api"].(string), param["prefix"].(string)+param["username"].(string))
	_, header := cq9Pack(param["api_token"].(string), nil)
	statusCode, body, err := httpGetHeaderWithLog(zlog, param["username"].(string), CQ9, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	status := v.Get("status")
	if string(status.GetStringBytes("code")) != "0" {
		return Failure, string(status.GetStringBytes("message"))
	}

	data := v.Get("data")
	return Success, GetBalanceFromFloat(data.GetFloat64("balance"))
}

// 上下分
func cq9Transfer(zlog *fluent.Fluent, param map[string]interface{}) (int, string) {

	var p fastjson.Parser

	args := map[string]string{
		"account": param["prefix"].(string) + param["username"].(string),
		"mtcode":  param["id"].(string),
		"amount":  param["amount"].(string),
	}
	flags := "withdraw"
	if param["type"].(string) == "in" {
		flags = "deposit"
	}
	requestURI := fmt.Sprintf("%s/gameboy/player/%s", param["api"].(string), flags)
	requestBody, header := cq9Pack(param["api_token"].(string), args)
	statusCode, body, err := httpPostWithPushLog(zlog, []byte(requestBody), param["username"].(string), CQ9, requestURI, header)
	if err != nil {
		return Failure, err.Error()
	}

	if statusCode != fasthttp.StatusOK {
		return Failure, fmt.Sprintf("%d", statusCode)
	}

	v, err := p.ParseBytes(body)
	if err != nil {
		return Failure, err.Error()
	}

	status := v.Get("status")
	if string(status.GetStringBytes("code")) == "0" {
		return Success, param["id"].(string)
	}

	return Failure, string(status.GetStringBytes("message"))
}
